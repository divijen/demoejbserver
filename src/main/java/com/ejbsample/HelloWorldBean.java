package com.ejbsample;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

@Stateless(name = "HelloWorld")
public class HelloWorldBean implements HelloWorld {

    @Resource
    private SessionContext context;

    public String getHelloWorld() {
        return "Welcome to EJB Tutorial!";
    }
}
