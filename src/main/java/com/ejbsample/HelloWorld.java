package com.ejbsample;

import javax.ejb.Remote;

/**
 * Hello world!
 *
 */
@Remote
public interface HelloWorld {
    String getHelloWorld();
}
